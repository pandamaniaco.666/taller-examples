package miproyecto;

import java.util.Calendar;
import java.util.List;
import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;

import com.google.gson.Gson;

import miproyecto.modelos.Usuario;

public class Utils {
	
	public static int StatusCodeNoLogged = 401;
	public final static String DefaultErrorMessage = "{\"error\":\"no session\"}";

	public static int currentYear() {
		return Calendar.getInstance().get(Calendar.YEAR);
	}
	
	public static Date currentDate() {
		return new Date((Calendar.getInstance().getTime()).getTime());
	}

	public static String toLocalFormat(Date fecha){
		if(fecha == null)
			return null;
		return (new SimpleDateFormat("dd/MM/yyyy")).format(fecha);
	}
	
	public static Usuario getUsuarioSession(HttpSession session) {
		String user = session.getAttribute("usuario").toString();
		return new Gson().fromJson(user, Usuario.class);
	}
	
	public static boolean verificarSession(Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);
		if(session == null) {
			model.addAttribute("error", "no session");
			response.setStatus(StatusCodeNoLogged);
			return false;
		} else 
			if(session.getAttribute("logged") == null) {
				response.setStatus(StatusCodeNoLogged);
				return false;
			}
		
		return true;
	}
	
	public static boolean verificarSession(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);
		if(session == null) {
			response.setStatus(StatusCodeNoLogged);
			return false;
		}
		else if(session.getAttribute("logged") == null){
			response.setStatus(StatusCodeNoLogged);
			return false;
		}
		return true;
	}
	
	public static String respuestaJSON(List<String> mensages) {
		return "{\"estado\" : \""+(mensages.isEmpty()?"success":"error")+"\", \"messages\":"+mensages.toString()+"}";
	}
	
	public static String prepareToRead(String str) {
		if(str == null)
			return null;
		
		return str.replaceAll("\"", "\\\"");
	}
	
}