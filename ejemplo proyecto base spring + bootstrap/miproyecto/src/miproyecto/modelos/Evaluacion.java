package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Evaluacion  extends BasicObject{
	
	int idprog;
	int idexamen;
	Short nota;
	
	public int getIdprog() {
		return idprog;
	}

	public void setIdprog(int idprog) {
		this.idprog = idprog;
	}

	public int getIdexamen() {
		return idexamen;
	}

	public void setIdexamen(int idexamen) {
		this.idexamen = idexamen;
	}

	public Short getNota() {
		return nota;
	}

	public void setNota(Short nota) {
		this.nota = nota;
	}

	public boolean isValid() {
		return	getIdprog() > 0
				&& getIdexamen() > 0;
	}

	public static RowMapper<Evaluacion> getRowMapper() {
		return new RowMapper<Evaluacion>() {
			@Override
			public Evaluacion mapRow(ResultSet rs, int c) throws SQLException {
				Evaluacion e = new Evaluacion();
				e.setIdprog(rs.getInt("idprog"));
				e.setIdexamen(rs.getInt("idexamen"));
				e.setNota(rs.getShort("nota"));
				return e;
			}
		};
	}

}
