package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Proceso extends BasicObject{
	
	int idpro;
	String nombre;
	String enlace;
	String descripcion;

	public int getIdpro() {
		return idpro;
	}

	public void setIdpro(int idpro) {
		this.idpro = idpro;
	}

	public String getEnlace() {
		return enlace;
	}

	public void setEnlace(String enlace) {
		this.enlace = enlace;
	}

	public String getDescripcion() {
		return descripcion != null ? descripcion : "";
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean isValid() {
		return	getIdpro() > 0
				&& getNombre() != null && getNombre().length() >= 2 && getNombre().length() <= 40
				&& getEnlace() != null && getEnlace().length() >= 1 && getEnlace().length() <= 40
				&& (descripcion == null || (getDescripcion().length() >= 2 && getDescripcion().length() <= 60));
	}

	public static RowMapper<Proceso> getRowMapper() {
		return new RowMapper<Proceso>() {
			@Override
			public Proceso mapRow(ResultSet rs, int c) throws SQLException {
				Proceso p = new Proceso();
				p.setIdpro(rs.getInt("idpro"));
				p.setNombre(rs.getString("nombre"));
				p.setEnlace(rs.getString("enlace"));
				p.setDescripcion(rs.getString("descripcion"));
				return p;
			}
		};
	}

}
