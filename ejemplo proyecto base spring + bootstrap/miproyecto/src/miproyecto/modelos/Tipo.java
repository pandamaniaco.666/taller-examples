package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Tipo  extends BasicObject{
	
	int idtipo;
	String nombre;
	boolean activo;

	public int getIdtipo() {
		return idtipo;
	}

	public void setIdtipo(int idtipo) {
		this.idtipo = idtipo;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean isValid() {
		return	getIdtipo() > 0
				&& getNombre() != null && getNombre().length() >= 3 && getNombre().length() <= 40;
	}

	public static RowMapper<Tipo> getRowMapper() {
		return new RowMapper<Tipo>() {
			@Override
			public Tipo mapRow(ResultSet rs, int c) throws SQLException {
				Tipo t = new Tipo();
				t.setIdtipo(rs.getInt("idtipo"));
				t.setNombre(rs.getString("nombre"));
				t.setActivo(rs.getBoolean("activo"));
				return t;
			}
		};
	}

}