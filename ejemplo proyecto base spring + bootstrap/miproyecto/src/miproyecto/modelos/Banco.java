package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Banco  extends BasicObject{
	
	int idbanco;
	String enunciado;
	String imagen;
	short tema;
	Short subtema;
	String dificultad;
	boolean activo;
	Integer iddicta;
	Integer idtipo;

	public int getIdbanco() {
		return idbanco;
	}

	public void setIdbanco(int idbanco) {
		this.idbanco = idbanco;
	}

	public String getEnunciado() {
		return enunciado;
	}

	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}

	public String getImagen() {
		return imagen != null? imagen: "";
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public short getTema() {
		return tema;
	}

	public void setTema(short tema) {
		this.tema = tema;
	}

	public Short getSubtema() {
		return subtema;
	}

	public void setSubtema(Short subtema) {
		this.subtema = subtema;
	}

	public String getDificultad() {
		return dificultad;
	}

	public void setDificultad(String dificultad) {
		this.dificultad = dificultad;
	}

	public Integer getIddicta() {
		return iddicta;
	}

	public void setIddicta(Integer iddicta) {
		this.iddicta = iddicta;
	}

	public Integer getIdtipo() {
		return idtipo;
	}

	public void setIdtipo(Integer idtipo) {
		this.idtipo = idtipo;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public boolean isValid() {
		return getIdbanco() > 0
				&& getEnunciado() != null && getEnunciado().length() >= 2 && getEnunciado().length() <= 300
				&& (imagen == null || (getImagen().length() >= 2 && getImagen().length() <= 60))
				&& getTema() > 0
				&& getDificultad() != null && getDificultad().length() == 1;
	}

	public static RowMapper<Banco> getRowMapper() {
		return new RowMapper<Banco>() {
			@Override
			public Banco mapRow(ResultSet rs, int c) throws SQLException {
				Banco b = new Banco();
				b.setIdbanco(rs.getInt("idbanco"));
				b.setEnunciado(rs.getString("enunciado"));
				b.setImagen(rs.getString("imagen"));
				b.setTema(rs.getShort("tema"));
				b.setSubtema(rs.getShort("subtema"));
				b.setDificultad(rs.getString("dificultad"));
				b.setIddicta(rs.getInt("iddicta"));
				b.setIdtipo(rs.getInt("idtipo"));
				b.setActivo(rs.getBoolean("activo"));
				return b;
			}
		};
	}

}
