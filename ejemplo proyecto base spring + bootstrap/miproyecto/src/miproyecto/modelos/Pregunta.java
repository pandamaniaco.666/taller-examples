package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Pregunta  extends BasicObject{
	
	int idexamen;
	int idbanco;
	short ponderacion;

	public int getIdexamen() {
		return idexamen;
	}

	public void setIdexamen(int idexamen) {
		this.idexamen = idexamen;
	}

	public int getIdbanco() {
		return idbanco;
	}

	public void setIdbanco(int idbanco) {
		this.idbanco = idbanco;
	}

	public short getPonderacion() {
		return ponderacion;
	}

	public void setPonderacion(short ponderacion) {
		this.ponderacion = ponderacion;
	}

	public boolean isValid() {
		return	getIdexamen() > 0
				&& getIdbanco() > 0;
	}

	public static RowMapper<Pregunta> getRowMapper() {
		return new RowMapper<Pregunta>() {
			@Override
			public Pregunta mapRow(ResultSet rs, int c) throws SQLException {
				Pregunta p = new Pregunta();
				p.setIdexamen(rs.getInt("idexamen"));
				p.setIdbanco(rs.getInt("idbanco"));
				p.setPonderacion(rs.getShort("ponderacion"));
				return p;
			}
		};
	}

}
