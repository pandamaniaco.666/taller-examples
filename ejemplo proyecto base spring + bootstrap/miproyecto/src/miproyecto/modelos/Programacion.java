package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Programacion  extends BasicObject{
	
	int idprog;
	Integer idusu;
	Integer idcarr;
	String sigla;
	String gestion;

	public int getIdprog() {
		return idprog;
	}

	public void setIdprog(int idprog) {
		this.idprog = idprog;
	}

	public Integer getIdusu() {
		return idusu;
	}

	public void setIdusu(Integer idusu) {
		this.idusu = idusu;
	}

	public Integer getIdcarr() {
		return idcarr;
	}

	public void setIdcarr(Integer idcarr) {
		this.idcarr = idcarr;
	}

	public String getSigla() {
		return sigla != null? sigla: "";
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getGestion() {
		return gestion != null? gestion: "";
	}

	public void setGestion(String gestion) {
		this.gestion = gestion;
	}

	public boolean isValid() {
		return	idprog > 0
				&& (sigla == null || getSigla().length() == 6)
				&& (gestion == null || getGestion().length() == 4);
	}

	public static RowMapper<Programacion> getRowMapper() {
		return new RowMapper<Programacion>() {
			@Override
			public Programacion mapRow(ResultSet rs, int c) throws SQLException {
				Programacion p = new Programacion();
				p.setIdprog(rs.getInt("idprog"));
				p.setIdusu(rs.getInt("idusu"));
				p.setIdcarr(rs.getInt("idcarr"));
				p.setSigla(rs.getString("sigla"));
				p.setGestion(rs.getString("gestion"));
				return p;
			}
		};
	}

}
