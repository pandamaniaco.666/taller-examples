package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Datos extends BasicObject {
	
	int idusu;
	String login;
	String password;
	boolean activo;
	
	public int getIdusu() {
		return idusu;
	}
	public void setIdusu(int idusu) {
		this.idusu = idusu;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isValid() {
		return getIdusu() > 0 && getIdusu() < 99999999 && isValidL();
	}
	
	public boolean isValidL() {
		return	getLogin() != null && getPassword() != null
				&& getLogin().length() >= 4 && getPassword().length() >= 4
				&& getLogin().length() <= 15 && getPassword().length() <= 100;
	}
	
	public static RowMapper<Datos> getRowMapper() {
		return new RowMapper<Datos>() {
			@Override
			public Datos mapRow(ResultSet rs, int c) throws SQLException {
				Datos d = new Datos();
				d.setIdusu(rs.getInt("idusu"));
				d.setLogin(rs.getString("login"));
				d.setPassword(rs.getString("password"));
				d.setActivo(rs.getBoolean("activo"));
				return d;
			}
		};
	}

}
