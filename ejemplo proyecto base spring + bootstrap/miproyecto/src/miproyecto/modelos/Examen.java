package miproyecto.modelos;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

import org.springframework.jdbc.core.RowMapper;

public class Examen  extends BasicObject{
	
	int idexamen;
	String nombre;
	Date fecha;
	Time hora_ini;
	Time hora_fin;
	short penalizacion;
	Integer iddicta;
	boolean activo;

	public int getIdexamen() {
		return idexamen;
	}

	public void setIdexamen(int idexamen) {
		this.idexamen = idexamen;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Time getHora_ini() {
		return hora_ini;
	}

	public void setHora_ini(Time hora_ini) {
		this.hora_ini = hora_ini;
	}

	public Time getHora_fin() {
		return hora_fin;
	}

	public void setHora_fin(Time hora_fin) {
		this.hora_fin = hora_fin;
	}

	public short getPenalizacion() {
		return penalizacion;
	}

	public void setPenalizacion(short penalizacion) {
		this.penalizacion = penalizacion;
	}

	public Integer getIddicta() {
		return iddicta;
	}

	public void setIddicta(Integer iddicta) {
		this.iddicta = iddicta;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean isValid() {
		return	getIdexamen() > 0
				&& getNombre() != null && getNombre().length() >= 3 && getNombre().length() <= 15
				&& getPenalizacion()> 0;
	}

	public static RowMapper<Examen> getRowMapper() {
		return new RowMapper<Examen>() {
			@Override
			public Examen mapRow(ResultSet rs, int c) throws SQLException {
				Examen e = new Examen();
				e.setIdexamen(rs.getInt("idexamen"));
				e.setNombre(rs.getString("nombre"));
				e.setFecha(rs.getDate("fecha"));
				e.setHora_ini(rs.getTime("hora_ini"));
				e.setHora_fin(rs.getTime("hora_fin"));
				e.setPenalizacion(rs.getShort("penalizacion"));
				e.setIddicta(rs.getInt("iddicta"));
				e.setActivo(rs.getBoolean("activo"));
				return e;
			}
		};
	}

}
