package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Opcion  extends BasicObject{
	
	int idopcion;
	Integer idbanco;
	String opcion;
	boolean correcta;

	public int getIdopcion() {
		return idopcion;
	}

	public void setIdopcion(int idopcion) {
		this.idopcion = idopcion;
	}

	public Integer getIdbanco() {
		return idbanco;
	}

	public void setIdbanco(Integer idbanco) {
		this.idbanco = idbanco;
	}

	public String getOpcion() {
		return opcion;
	}

	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	public boolean isCorrecta() {
		return correcta;
	}

	public void setCorrecta(boolean correcta) {
		this.correcta = correcta;
	}

	public boolean isValid() {
		return	getIdopcion() > 0
				&& getOpcion() != null && getOpcion().length() >= 2 && getOpcion().length() <= 100;
	}

	public static RowMapper<Opcion> getRowMapper() {
		return new RowMapper<Opcion>() {
			@Override
			public Opcion mapRow(ResultSet rs, int c) throws SQLException {
				Opcion o = new Opcion();
				o.setIdopcion(rs.getInt("idopcion"));
				o.setIdbanco(rs.getInt("idbanco"));
				o.setOpcion(rs.getString("opcion"));
				o.setCorrecta(rs.getBoolean("correcta"));
				return o;
			}
		};
	}

}