package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Dicta  extends BasicObject{
	
	int iddicta;
	Integer idusu;
	Integer idcarr;
	String sigla;
	String gestion;
	boolean activo;

	public int getIddicta() {
		return iddicta;
	}

	public void setIddicta(int iddicta) {
		this.iddicta = iddicta;
	}

	public Integer getIdusu() {
		return idusu;
	}

	public void setIdusu(Integer idusu) {
		this.idusu = idusu;
	}

	public Integer getIdcarr() {
		return idcarr;
	}

	public void setIdcarr(Integer idcarr) {
		this.idcarr = idcarr;
	}

	public String getSigla() {
		return sigla != null? sigla: "";
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getGestion() {
		return gestion != null ? gestion: "";
	}

	public void setGestion(String gestion) {
		this.gestion = gestion;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public boolean isValid() {
		return	getIddicta() > 0
				&& (sigla == null || getSigla().length() == 6)
				&& (gestion == null || getGestion().length() == 4);
	}

	public static RowMapper<Dicta> getRowMapper() {
		return new RowMapper<Dicta>() {
			@Override
			public Dicta mapRow(ResultSet rs, int c) throws SQLException {
				Dicta d = new Dicta();
				d.setIddicta(rs.getInt("iddicta"));
				d.setIdcarr(rs.getInt("idcarr"));
				d.setIdusu(rs.getInt("idusu"));
				d.setSigla(rs.getString("sigla"));
				d.setGestion(rs.getString("gestion"));
				d.setActivo(rs.getBoolean("activo"));
				return d;
			}
		};
	}

}
