package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Docente  extends BasicObject{
	
	int idusu;
	boolean auxiliar;

	public int getIdusu() {
		return idusu;
	}

	public void setIdusu(int idusu) {
		this.idusu = idusu;
	}

	public boolean isAuxiliar() {
		return auxiliar;
	}

	public void setAuxiliar(boolean auxiliar) {
		this.auxiliar = auxiliar;
	}

	public boolean isValid() {
		return	getIdusu() > 0;
	}

	public static RowMapper<Docente> getRowMapper() {
		return new RowMapper<Docente>() {
			@Override
			public Docente mapRow(ResultSet rs, int c) throws SQLException {
				Docente d = new Docente();
				d.setIdusu(rs.getInt("idusu"));
				d.setAuxiliar(rs.getBoolean("auxiliar"));
				return d;
			}
		};
	}

}
