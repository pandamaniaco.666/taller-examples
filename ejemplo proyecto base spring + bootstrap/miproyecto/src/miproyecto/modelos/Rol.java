package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Rol  extends BasicObject{
	
	int idrol;
	String nombre;
	boolean activo;

	public int getIdrol() {
		return idrol;
	}

	public void setIdrol(int idrol) {
		this.idrol = idrol;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean isValid() {
		return	getIdrol() > 0
				&& getNombre() != null && getNombre().length() >= 3 && getNombre().length() <= 40;
	}

	public static RowMapper<Rol> getRowMapper() {
		return new RowMapper<Rol>() {
			@Override
			public Rol mapRow(ResultSet rs, int c) throws SQLException {
				Rol r = new Rol();
				r.setIdrol(rs.getInt("idrol"));
				r.setNombre(rs.getString("nombre"));
				r.setActivo(rs.getBoolean("activo"));
				return r;
			}
		};
	}

}
