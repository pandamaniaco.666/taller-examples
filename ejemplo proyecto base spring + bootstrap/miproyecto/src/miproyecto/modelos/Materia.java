package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Materia  extends BasicObject{
	
	int idcarr;
	String nombre;
	String sigla;
	short cargahoraria;
	String plan;
	String periodo;
	String paralelo;
	boolean activo;

	public int getIdcarr() {
		return idcarr;
	}

	public void setIdcarr(int idcarr) {
		this.idcarr = idcarr;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public short getCargahoraria() {
		return cargahoraria;
	}

	public void setCargahoraria(short cargahoraria) {
		this.cargahoraria = cargahoraria;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getParalelo() {
		return paralelo;
	}

	public void setParalelo(String paralelo) {
		this.paralelo = paralelo;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean isValid() {
		return	getIdcarr() > 0
				&& getSigla() != null && getSigla().length() == 6
				&& getNombre() != null && getNombre().length() >= 3 && getNombre().length() <= 40
				&& getPlan() != null && getPlan().length() <= 4
				&& getPeriodo() != null && getPeriodo().length() == 1
				&& getParalelo() != null && getParalelo().length() == 1;
	}

	public static RowMapper<Materia> getRowMapper() {
		return new RowMapper<Materia>() {
			@Override
			public Materia mapRow(ResultSet rs, int c) throws SQLException {
				Materia m = new Materia();
				m.setIdcarr(rs.getInt("idcarr"));
				m.setSigla(rs.getString("sigla"));
				m.setNombre(rs.getString("nombre"));
				m.setCargahoraria(rs.getShort("cargahoraria"));
				m.setPlan(rs.getString("plan"));
				m.setPeriodo(rs.getString("periodo"));
				m.setParalelo(rs.getString("paralelo"));
				m.setActivo(rs.getBoolean("activo"));
				return m;
			}
		};
	}

}