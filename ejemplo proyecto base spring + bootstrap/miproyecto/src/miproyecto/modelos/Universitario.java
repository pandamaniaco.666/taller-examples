package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Universitario  extends BasicObject{
	
	int idusu;
	int ru;

	public int getIdusu() {
		return idusu;
	}

	public void setIdusu(int idusu) {
		this.idusu = idusu;
	}

	public int getRu() {
		return ru;
	}

	public void setRu(int ru) {
		this.ru = ru;
	}

	public boolean isValid() {
		return	getIdusu() > 0
				&& getRu() > 0;
	}

	public static RowMapper<Universitario> getRowMapper() {
		return new RowMapper<Universitario>() {
			@Override
			public Universitario mapRow(ResultSet rs, int c) throws SQLException {
				Universitario u = new Universitario();
				u.setIdusu(rs.getInt("idusu"));
				u.setRu(rs.getInt("ru"));
				return u;
			}
		};
	}

}
