package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Menu extends BasicObject {
	
	int idmenu;
	String nombre;
	String enlace = "";
	boolean activo;

	public int getIdmenu() {
		return idmenu;
	}

	public void setIdmenu(int idmenu) {
		this.idmenu = idmenu;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEnlace() {
		return enlace;
	}

	public void setEnlace(String enlace) {
		this.enlace = enlace;
	}

	@Override
	public boolean isValid() {
		return getIdmenu() > 0
				&& getNombre() != null && getNombre().length() >= 3 && getNombre().length() <= 40
				&& enlace != null;
	}
	
	public static RowMapper<Menu> getRowMapper() {
		return new RowMapper<Menu>() {
			@Override
			public Menu mapRow(ResultSet rs, int c) throws SQLException {
				Menu m = new Menu();
				m.setIdmenu(rs.getInt("idmenu"));
				m.setNombre(rs.getString("nombre"));
				m.setEnlace(rs.getString("enlace"));
				m.setActivo(rs.getBoolean("activo"));
				return m;
			}
		};
	}

}