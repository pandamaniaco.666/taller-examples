package miproyecto.modelos;

import java.sql.Array;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Usuario extends BasicObject{
	
	int idusu;
	String nombre;
	String apellido1;
	String apellido2;
	String sexo;
	Date f_nac;
	String cedula;
	String telefono;
	String direccion;
	String foto;
	boolean activo;
	String[] roles = new String[] {};
	
	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		if(roles == null)
			roles = new String[] {};
		this.roles = roles;
	}

	public int getIdusu() {
		return idusu;
	}

	public void setIdusu(int idusu) {
		this.idusu = idusu;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return (this.apellido2 == null? "" : this.apellido2);
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getSexo() {
		return (sexo != null? sexo: "").toUpperCase();
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Date getF_nac() {
		return f_nac;
	}

	public void setF_nac(Date f_nac) {
		this.f_nac = f_nac;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getTelefono() {
		return (telefono == null? "" : telefono);
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return (direccion  == null? "" : direccion);
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getNombreCompleto() {
		return getNombre() + " "
				+ getApellido1() + " "
				+ getApellido2();
	}
	
	@Override
	public boolean isValid() {
		return getIdusu() > 0
				&& getNombre() != null && getNombre().length() >= 2 && getNombre().length() <= 40
				&& getApellido1() != null && getApellido1().length() >= 2 && getApellido1().length() <= 40
				&& (apellido2 == null || (getApellido2().length() >= 2 && getApellido2().length() <= 40))
				&& sexo != null && (getSexo().equals("M") || getSexo().equals("F"))
				&& getF_nac() != null
				&& getCedula() != null && getCedula().length() >= 6 && getCedula().length() <= 15
				&& (getTelefono().length() == 0 || (getTelefono().length() >= 5 && getTelefono().length() <= 15))
				&& (getDireccion().length() == 0 || (getDireccion().length() >= 5 && getDireccion().length() <= 60))
				&& getFoto() != null && getFoto().length() >= 2 && getFoto().length() <= 60;
	}

	public static RowMapper<Usuario> getRowMapper() {
		return new RowMapper<Usuario>() {
			@Override
			public Usuario mapRow(ResultSet rs, int c) throws SQLException {
				Usuario u = new Usuario();
				u.setIdusu(rs.getInt("idusu"));
				u.setNombre(rs.getString("nombre"));
				u.setApellido1(rs.getString("apellido1"));
				u.setApellido2(rs.getString("apellido2"));
				u.setSexo(rs.getString("sexo"));
				u.setF_nac(rs.getDate("f_nac"));
				u.setCedula(rs.getString("cedula"));
				u.setTelefono(rs.getString("telefono"));
				u.setDireccion(rs.getString("direccion"));
				u.setFoto(rs.getString("foto"));
				u.setActivo(rs.getBoolean("activo"));
				try {
					Array arr = rs.getArray("roles");
					u.setRoles((String[])arr.getArray());
				} catch (Exception e) {
					u.setRoles(null);
				}
				return u;
			}
		};
	}

}
