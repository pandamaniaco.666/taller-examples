package miproyecto.modelos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class Carrera  extends BasicObject{
	
	int idcarr;
	String nombre;
	String direccion;
	String telefono;
	boolean activo;

	public int getIdcarr() {
		return idcarr;
	}

	public void setIdcarr(int idcarr) {
		this.idcarr = idcarr;
	}

	public String getDireccion() {
		return direccion != null ?direccion : "";
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono != null? telefono:"";
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean isValid() {
		return	getIdcarr() > 0
				&& getNombre() != null && getNombre().length() >= 2 && getNombre().length() <= 40
				&& (direccion == null || (getDireccion().length() >= 2 && getDireccion().length() <= 60))
				&& (telefono == null || (getTelefono().length() >= 2 && getTelefono().length() <= 15));
	}

	public static RowMapper<Carrera> getRowMapper() {
		return new RowMapper<Carrera>() {
			@Override
			public Carrera mapRow(ResultSet rs, int i) throws SQLException {
				Carrera c = new Carrera();
				c.setIdcarr(rs.getInt("idcarr"));
				c.setNombre(rs.getString("nombre"));
				c.setDireccion(rs.getString("direccion"));
				c.setTelefono(rs.getString("telefono"));
				c.setActivo(rs.getBoolean("activo"));
				return c;
			}
		};
	}

}