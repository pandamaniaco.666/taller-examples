package miproyecto.modelos;

import com.google.gson.Gson;

public abstract class BasicObject {

	public abstract boolean isValid();
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}
