package miproyecto.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import miproyecto.servicios.UsuariosService;

@Controller
@RequestMapping("/usuarios**")
public class UsuariosController {
	
	@Autowired
	UsuariosService usuariosService;
	
	@RequestMapping("/lista.html")
	public String listarUsuarios(Model model) {
		model.addAttribute("lista", usuariosService.listar());
		return "usuarios/gestion";
	}
	
}
