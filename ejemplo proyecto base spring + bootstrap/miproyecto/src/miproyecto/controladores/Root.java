package miproyecto.controladores;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import miproyecto.modelos.Datos;
import miproyecto.modelos.Menu;
import miproyecto.modelos.Rol;
import miproyecto.modelos.Usuario;
import miproyecto.servicios.DatosService;
import miproyecto.servicios.MenusService;
import miproyecto.servicios.RolesService;

import static miproyecto.utils.UResponse.*;

@Controller
public class Root {
	
	@Autowired
	DatosService datosService;
	
	@Autowired
	RolesService rolesService;
	
	@Autowired
	MenusService menusService;

	@RequestMapping(value = "/index.html", method = RequestMethod.GET)
	public String login(HttpServletResponse response) {
		try{
			response.sendRedirect(FullRoot);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/")
	public String index(Model model, HttpServletRequest request, HttpServletResponse response){
		HttpSession session = request.getSession(true);
		Object usuario = session.getAttribute("usuario");
		if(usuario != null) {
			Usuario usuario2 = (Usuario)usuario;
			List<Rol> roles = rolesService.lista_roles_usuario(usuario2);
			List<Menu> menus = new ArrayList<Menu>();
			if(roles.size() > 0)
				menus = menusService.lista_menus_rol(roles.get(0));
			
			model.addAttribute("usuario", usuario2);
			model.addAttribute("roles", roles);
			model.addAttribute("menus", menus);
			return "principal";
		}else{
			try{
				response.sendRedirect(FullRoot+"login.html");
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/login.html", method = RequestMethod.GET)
	public String login(Model model) {
		return "login";
	}
	
	@RequestMapping(value = "/login.html", method = RequestMethod.POST)
	public String validate(Model model, HttpServletRequest request, HttpServletResponse responce, Datos datos) {
		HttpSession session = request.getSession(true);
		System.out.println(datos);
		
		if(datos.isValidL()) {
			Usuario u;
			if((u = datosService.validarUsuario(datos)) != null) {
				session.setAttribute("logged", true);
				session.setAttribute("usuario", u);
				try {
					responce.sendRedirect(FullRoot);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}else
				try {
					responce.sendRedirect("login.html?error=norecord");
				} catch (IOException e) {
					e.printStackTrace();
				}
		}else
			try {
				responce.sendRedirect("login.html?error=novalid");
			} catch (IOException e) {
				e.printStackTrace();
			}	
		return null;
	}
	
	@RequestMapping("/logout.html")
	public String logout(HttpServletRequest request, HttpServletResponse response){
		HttpSession session = request.getSession(true);
		try {
			session.invalidate();
			response.sendRedirect(FullRoot);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
