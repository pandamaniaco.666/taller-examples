package miproyecto.servicios;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import miproyecto.modelos.Menu;
import miproyecto.modelos.Rol;

@Service
public class MenusService{
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public Menu verMenu(int idmenu) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM menus where idmenu = ?;", new Object[] {idmenu}, Menu.getRowMapper());
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return null;
		}
	}
	
	public List<Menu> lista_menus(){
		try {
			return jdbcTemplate.query("SELECT * FROM menus;", Menu.getRowMapper());
		} catch (Exception e) {
			return new ArrayList<Menu>();
		}
	}
	
	public List<Menu> lista_menus_rol(Rol rol){
		try {
			if(rol == null) throw new NullPointerException();
			return jdbcTemplate.query("select m.* from menus m " + 
					"where m.idmenu in (select idmenu from rolme " + 
					"			where idrol = ? " + 
					"		) and m.activo = true;", new Object[] {rol.getIdrol()}, Menu.getRowMapper());
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return new ArrayList<Menu>();
		}
	}
	
	public boolean adicionarMenu(Menu menu) {
		try {
			return jdbcTemplate.update("inset into menus values(default,?,true);", new Object[] {menu.getNombre()}) == 0;
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return false;
	}

	public boolean modificarMenu(Menu menu) {
		try {
			return jdbcTemplate.queryForObject("update menus set nombre=? where idmenu=?;", new Object[] {
					menu.getNombre(),
					menu.getIdmenu()
					}, Boolean.class);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return false;
	}

	public boolean eliminarMenu(int idmenu, boolean estado) {
		try {
			return jdbcTemplate.update("update menus set activo = ? where idmenu= ?;", new Object[] {estado, idmenu}) == 0;
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return false;
	}

	public boolean asignarProceso(int idmenu, int proceso) {
		try {
			return jdbcTemplate.update("insert into mepro values(?,?);", new Object[]{idmenu, proceso}) == 0;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	public boolean reasignarMenus(int idmenu) {
		try {
			return jdbcTemplate.update("delete from mepro where idmenu= ?;", new Object[]{ idmenu }) == 0;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

}