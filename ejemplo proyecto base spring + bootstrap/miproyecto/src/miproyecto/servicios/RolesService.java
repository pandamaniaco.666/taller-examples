package miproyecto.servicios;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import miproyecto.modelos.Rol;
import miproyecto.modelos.Usuario;

@Service
public class RolesService{
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource){
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public Rol verRol(int idrol) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM roles where idrol = ?;", new Object[] {idrol}, Rol.getRowMapper());
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return null;
		}
	}
	
	public List<Rol> lista_roles(){
		try {
			return jdbcTemplate.query("SELECT * FROM roles;", Rol.getRowMapper());
		} catch (Exception e) {
			return new ArrayList<Rol>();
		}
	}

	public List<Rol> lista_roles_usuario(Usuario usuario){
		try {
			if(usuario == null) throw new NullPointerException();
			return jdbcTemplate.query("select r.* from roles r " + 
					"where r.idrol in (select idrol from usurol " + 
					"			where idusu = ? " + 
					"		) and r.activo = true;", new Object[] {usuario.getIdusu()}, Rol.getRowMapper());
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return new ArrayList<Rol>();
		}
	}

	public boolean adicionarRol(Rol rol) {
		try {
			return jdbcTemplate.update("inset into roles values(default,?,true);", new Object[] {rol.getNombre()}) == 0;
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return false;
	}

	public boolean modificarRol(Rol rol) {
		try {
			return jdbcTemplate.queryForObject("update roles set nombre=? where idrol=?;", new Object[] {
					rol.getNombre(),
					rol.getIdrol()
					}, Boolean.class);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return false;
	}

	public boolean eliminarRol(int idrol, boolean estado) {
		try {
			return jdbcTemplate.update("update roles set activo = ? where idrol= ?;", new Object[] {estado, idrol}) == 0;
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return false;
	}

	public boolean asignarMenu(int idrol, int menu) {
		try {
			return jdbcTemplate.update("insert into rolme values(?,?);", new Object[]{idrol, menu}) == 0;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	public boolean reasignarMenus(int idrol) {
		try {
			return jdbcTemplate.update("delete from rolme where idrol= ?;", new Object[]{ idrol }) == 0;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

}