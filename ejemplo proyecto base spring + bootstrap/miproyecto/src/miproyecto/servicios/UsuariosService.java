package miproyecto.servicios;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import miproyecto.modelos.Usuario;

@Service
public class UsuariosService {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource){
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public List<Usuario> listar(){
		try {
			return jdbcTemplate.query("SELECT u.*, (SELECT array_agg(nombre) FROM roles r, usurol ur WHERE r.idrol=ur.idrol and ur.idusu=u.idusu) as roles FROM usuarios u;", Usuario.getRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}
	
}