package miproyecto.servicios;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import miproyecto.modelos.Datos;
import miproyecto.modelos.Usuario;

@Service
public class DatosService{
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource){
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public Usuario validarUsuario(Datos datos) {
		try {
			return jdbcTemplate.queryForObject("select * from usuarios u " + 
					"where u.idusu = (select d.idusu from datos d " + 
					"		where d.login = ? and d.password=md5(?)) " + 
					"		and activo=true " + 
					"limit 1;", new Object[] {datos.getLogin(), datos.getPassword()}, Usuario.getRowMapper());
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	public Datos verDatos(int idusu) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM datos where idusu = ?;", new Object[] { idusu }, Datos.getRowMapper());
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	public boolean adicionarDatos(Datos datos) {
		try {
			return jdbcTemplate.update("insert into datos values(?,?,?,true);",
					new Object[] {
							datos.getIdusu(),
							datos.getLogin(),
							datos.getPassword()
					}) == 0;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	public boolean modificarDatos(Datos datos) {
		try {
			return jdbcTemplate.update("update datos set password=md5(?) where idusu = ? and login = ?;",
					new Object[] {
							datos.getPassword(),
							datos.getIdusu(),
							datos.getLogin()
					}) == 0;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return false;
		}
	}
	
	public boolean eliminarDatos(int idusu, boolean estado) {
		try {
			return jdbcTemplate.update("update datos set activo=? where idusu=?;", new Object[] { estado, idusu })
					== 0;
		} catch (Exception e) {
			return false;
		}
	}
	
}
