# Taller Examples

Ejemplos para la materia de taller y otros.

## Distribucion de archivos o Estructura del proyecto:

```
Leyenda:
+ : Carpeta
- : Archivo


+ workspace
	+ miproyecto
		+ .settings
			- archivos de eclipse...
			- archivos de eclipse...
		+ build								        //se crea automaticamente
			+ classes
				+ miproyecto
					+ carpetas ...
					- codigo compilado....
		+ src
			+ miproyecto							//nombre package base
				+ controladores
					- Root.java				        // Rutas absolutas publicas ej. "/login.html", "logout.html", "/404.html"
					- Principal.java				// Rutas absolutas privadas ej. "/principal.html", "/cambiarRol.html", "/menus.html"
					- Carreras.java					// Rutas Relativas privadas ej. "/carreras/listar.html", "/carreras/adicionar.html", etc...
					- Menus.java					// Rutas Relativas privadas ej. "/menus/listar.html", "/menus/adicionar.html", etc...
					- demas rutas(Controladores) de mi proyecto...
				+ servicios
					- DatosService.java				//Ejecuta consultas a la DB relacionadas con la tabla "Datos"
					- MenusService.java				//Ejecuta consultas a la DB relacionadas con la tabla "Menus"
					- CarrerasService.java			        //lo mismo
					- UsuariosService.java			        //otravez
					- demas servicios para mi proyecto...
				+ modelos
					- Datos.java
					- Menu.java
					- Usuario.java
					- Carrera.java
					- demas modelos o tablas de la base de datos...
				+ utils
					- UDate.java
					- USession.java
					- UObjectToJSON.java
					- demas clases de ayuda...
		+ WebContent
			+ META-INF
				- MANIFEST.MF
			+ resources
				+ css								//hojas de estilo ".css"
					- miCss.css
				+ js								//codigo javascript ".js"
					- miScript.js
				+ images							//imagenes ".jpg, .png, .gif"
					- miImagen.jpg
				+ upload							//Carpeta donde se suben archivos publicos
					- fotoPerfil.jpg
				- 404.html							//Pagina de Error ( Not Found )
				- favicon.ico                                   // Icono de la pagina
			+ vistas
				+ carreras
					- gestion.vm
					- adicionar.vm
					- modificar.vm
					- ver.vm
				+ usuarios
					- gestion.vm
					- adicionar.vm
					- modificar.vm
					- ver.vm
				+ menus
					- gestion.vm
					- adicionar.vm
					- modificar.vm
					- ver.vm
				+ demas carpetas...
				- login.vm
				- principal.vm
				- error500.vm
			+ WEB-INF
				+ lib
					- postgresql-x.x.x.jrex.jar
					- spring-*****-x.x.x.RELEASE.jar
					- velocity-x.x.jar
					- demas librerias .jar ....
				- applicationContext.xml
				- miproyecto-servlet.xml
				- web.xml
		- .classpath
		- .project
		- otros archivos.....
```